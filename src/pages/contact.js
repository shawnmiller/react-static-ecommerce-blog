import React from 'react';
import Helmet from 'react-helmet';
import Layout from '../layout/index';
import SEO from '../components/SEO/SEO';
import config from '../../data/SiteConfig';
import GoogleMap from '../components/GoogleMap'

class ContactPage extends React.Component {

	render () {
		const divStyle = {
			height: '400px',
			padding:'10px',
			border:'1px solid #e5e5e5'
		};
		const mapCanvas = {
			height: '380px'
		}
		return (
			<Layout>
				<Helmet title={`Contact | ${config.siteTitle}`} />
				<SEO title="Contact - United States Vaping Association" keywords={['USVA', 'THEUSVA']} />
				<section>
					<div className="container">
						<div className="row">
							<div className="col-md-4">
								<div className="left-contact">
									<h3>Few Words About Us</h3>
									<p>{config.aboutText}</p>
								</div>
								<div className="address-info">
									<p><i className="fa fa-globe color-theme" aria-hidden="true"> </i> {config.companyAddress}</p>
									<p><i className="fa fa-phone color-theme" aria-hidden="true"> </i> {config.companyTelephone}</p>
									<p><i className="fa fa-link color-theme" aria-hidden="true"> </i> {config.companySiteUrl}</p>
									<p><i className="fa fa-paper-plane color-theme" aria-hidden="true"> </i> {config.emailSupport}</p>
								</div>
							</div>{/*column*/}
							<div className="col-md-8">
								<div className="right-contact">
									<h3>Find Us On The Map</h3>
									<div className="map-warp" style={divStyle}>
										<GoogleMap id={'map-canvas'}  style={mapCanvas}/>
									</div>{/*map canvas*/}
									<div className="comment-form-warp">
										<h3>Drop Us a Line</h3>
										<form className="form-inline">
											<div className="left-form-comment">
												<div className="form-group">
													<input type="text" className="form-control" id="exampleInputName2" placeholder="Your Name..." />
												</div>
												<div className="form-group">
													<input type="email" className="form-control" id="exampleInputEmail2" placeholder="Your Email" />
												</div>
												<button type="submit" className="ot-btn btn-main-color btn-rounded bg-theme text-up white-text">SEND MESSAGE <i className="fa fa-arrow-circle-right" aria-hidden="true"></i>
												</button>
											</div>
											<div className="right-form-comment">
												<textarea name="message" id="textarea" className="form-control" rows="7" required="required" placeholder="Your Message"></textarea>
											</div>{/*text-area*/}
										</form>
									</div>{/*form*/}
								</div>
							</div>{/*column*/}
						</div>{/*row*/}
					</div>{/*container*/}
				</section>
			</Layout>
		);
	}
}

export default ContactPage;
