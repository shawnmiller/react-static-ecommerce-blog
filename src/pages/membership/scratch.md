							{product.items.map((item, index) => (
								<div className="product" key={index}>
									<div className="product-image">
										<img alt="" src={item.thumbnail} />
										<div className="product-overlay">
											<Link to={item.link}>
												{item.buttonText}
											</Link>
										</div>
									</div>
									<div className="product-description">
										<div className="product-category">{item.category}</div>
										<div className="product-title">
											<h3>
												<Link href="/" title="Legal Fund Donation">
													{item.title}
												</Link>
											</h3>
										</div>
										<div className="product-price">
											<ins>{item.price}</ins>
										</div>
										<div className="product-rate">
											<i className="fa fa-star-o"> </i>
											<i className="fa fa-star-o"> </i>
											<i className="fa fa-star-o"> </i>
											<i className="fa fa-star-o"> </i>
											<i className="fa fa-star-o"> </i>
										</div>
										<div className="product-reviews">
											<a title={item.customerReviews + ' customer reviews'}>{item.customerReviews + ' customer reviews'}</a>
										</div>
									</div>
								</div>
							))}
