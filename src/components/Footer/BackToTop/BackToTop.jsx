/**
 * Created by Shawn Miller
 * User: Developer
 * Project: react-static-ecom-blog
 * Date: 12/05/2019 11:59 PM
 * Relative Path: src/components/Footer/BackToTop/BackToTop.jsx
 */

/* Based on Dotted HTML template*/
import React from "react"

const BackToTop = (props) => (
		<a id="to-the-top" className="fixbtt"><i className="fa fa-chevron-up"></i></a>
)

export default BackToTop
