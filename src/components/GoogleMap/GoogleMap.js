/**
 * Created by S Miller <hereiamnow@gmail.com>
 * Date: 1/29/2020 12:24 PM
 * User: Developer
 * Project: https://react-static-ecommerce-blog.netlify.com
 * GitRepo: https://github.com/hereiamnow/react-static-ecommerce-blog.git

 * Relative Path: ${FILE_NAME}
 * Description:
 */
import React from 'react';
import GoogleMapReact from 'google-map-react';

const defaultProps = {
	center: {
		lat: 30.522160,
		lng: -97.830190,
	},
	zoom: 15,
};

const AnyReactComponent = ({text}) => <div>{text}</div>;

const GoogleMap = (props) => (
	<div id={props.id} style={props.style}>
		<GoogleMapReact
			bootstrapURLKeys={{key: 'AIzaSyALIDKuIvgtK4QD7qf-9rcrHU3q8W82Fok'}}
			defaultCenter={defaultProps.center}
			defaultZoom={defaultProps.zoom}
			>
			{/*<AnyReactComponent lat={30.522160} lng={-97.830190} text={'Some Text'} />*/}
		</GoogleMapReact>
	</div>
)

export default GoogleMap;
