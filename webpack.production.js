/**
 * Created by S Miller <hereiamnow@gmail.com>
 * User: Developer
 * Project: https://react-static-ecom-blog.netlify.com
 * GitRepo: https://github.com/hereiamnow/react-static-ecommerce-blog.git
 * Date: 12/22/2019 12:06 PM
 * Relative Path: webpack.production.js
 * Description:
 */

const Dotenv = require('dotenv-webpack')

module.exports = {
	plugins: [
		new Dotenv({
			path: `./.env.production`
		})
	]
}
